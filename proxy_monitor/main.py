#!/usr/bin/env python
# encoding: utf-8
from time import sleep

import requests
from requests.exceptions import Timeout, ProxyError, ConnectionError

from proxy_monitor import config as CONFIG
from proxy_monitor import postman


def test_proxy():
    wrong_proxies = []
    for proxy in CONFIG.PROXIES:
        tries = CONFIG.CONNECTION_TRIES
        while True:
            try:
                requests.get(CONFIG.TEST_URL, proxies=dict(https=proxy), timeout=CONFIG.CONNECTION_TIMEOUT)
                break
            except (Timeout, ProxyError, ConnectionError):
                if tries == 0:
                    wrong_proxies.append(proxy)
                    break
                else:
                    tries -= 1
    return wrong_proxies


def main():
    while True:
        wrong_proxies = test_proxy()
        if len(wrong_proxies) > 0:
            postman.send('Proxy monitor warning', '\n'.join(wrong_proxies))
        sleep(60 * CONFIG.LOOP_TIME_INTERVAL)


if __name__ == '__main__':
    main()
